<?php

use KHerGe\JSON\JSON;
use PHPUnit\Framework\TestCase;
use Ucc\Services\QuestionService;

final class QuestionServiceTest extends TestCase
{
    private QuestionService $service;

    public function setUp(): void
    {
        $this->service = new QuestionService(new JSON(), new JsonMapper());
    }

    public function testQuestionsCanBeLoaded()
    {
        $questions = $this->service->getRandomQuestions();

        $this->assertNotEmpty($questions);
    }

    public function testItLoadsRightAmountOfQuestions()
    {
        // TODO : test zero or greater than total amount of questions
        for ($i = 1; $i <= 5; $i++) {
            $questions = $this->service->getRandomQuestions($i);

            $this->assertEquals($i, count($questions), "Asked to load $i questions, but loaded " . count($questions));
        }
    }
}

<?php

namespace Ucc\Controllers;

use Ucc\Models\Question;
use Ucc\Services\QuestionService;
use Ucc\Session;

class QuestionsController extends Controller
{
    private QuestionService $questionService;

    public function __construct(QuestionService $questionService)
    {
        parent::__construct();

        $this->questionService = $questionService;
    }

    public function beginGame(): void
    {
        $name = $this->requestBody->name ?? null;

        if (empty($name)) {
            $this->jsonResponse('You must provide a name', 400);
        }

        $questions = $this->getRandomQuestions();

        Session::set('name', $name);
        Session::set('points', 0);
        Session::set('questionCount', 1);
        Session::set('questions', serialize($questions));

        $question = array_values($questions)[0];

        $this->jsonResponse(['question' => $question], 201);
    }

    public function answerQuestion(int $id): void
    {
        if (Session::get('name') === null) {
            $this->jsonResponse('You must first begin a game', 400);
        }

        $currentQuestion = $this->getCurrentQuestion();

        if ($id !== $currentQuestion->getId()) {
            $this->jsonResponse([
                'message' => "That's awesome, but it's not what I asked. Other than that, awesome, yeah.",
                'question' => $currentQuestion,
            ], 418);
        }

        $answer = $this->requestBody->answer ?? null;

        if (empty($answer)) {
            $this->jsonResponse('You must provide an answer', 400);
        }

        $points = $this->questionService->getPointsForAnswer($id, $answer);

        Session::set('points', Session::get('points') + $points);

        if ($this->isLastQuestion()) {
            $this->finishGame();
        } else {
            $this->sendNextQuestion($points > 0);
        }
    }

    private function getRandomQuestions(): array
    {
        $questions = $this->questionService->getRandomQuestions(5);

        $indexedQuestions = [];

        foreach ($questions as $question) {
            $indexedQuestions[$question->getId()] = $question;
        }

        return $indexedQuestions;
    }

    private function sendNextQuestion(bool $correctAnswer): void
    {
        $currentCount = Session::get('questionCount');

        Session::set('questionCount', $currentCount + 1);

        $message = $correctAnswer ? 'That is CORRECT! How did you know?' : 'Try google next time';

        $questions = array_values($this->getQuestions());

        $this->jsonResponse(['message' => $message, 'question' => $questions[$currentCount]]);
    }

    private function isLastQuestion(): bool
    {
        return (int) Session::get('questionCount') > 4;
    }

    private function finishGame()
    {
        $name = Session::get('name');
        $points = Session::get('points');

        Session::destroy();

        return $this->jsonResponse([
            'message' => "Thank you for playing {$name}. Your total score was: {$points} points!"
        ]);
    }

    private function getQuestions()
    {
        return unserialize(Session::get('questions'));
    }

    private function getCurrentQuestion(): ?Question
    {
        $questions = array_values($this->getQuestions());

        return $questions[Session::get('questionCount') - 1] ?? null;
    }
}

<?php

namespace Ucc\Services;

use JsonMapper;
use KHerGe\JSON\JSON;
use Ucc\Models\Question;
use Ucc\Session;

class QuestionService
{
    private const QUESTIONS_PATH = __DIR__ . '/../../questions.json';

    private JSON $json;
    private JsonMapper $jsonMapper;

    public function __construct(JSON $json, JsonMapper $jsonMapper)
    {
        $this->json = $json;
        $this->jsonMapper = $jsonMapper;
    }

    public function getRandomQuestions(int $count = 5): array
    {
        $questionsJson = file_get_contents(self::QUESTIONS_PATH);
        $questionsObject = $this->json->decode($questionsJson);

        $questions = $this->jsonMapper->mapArray($questionsObject, [], Question::class);

        shuffle($questions);

        return array_slice($questions, 0, $count);
    }

    public function getPointsForAnswer(int $id, string $answer): int
    {
        $questions = unserialize(Session::get('questions'));

        $correctAnswer = $questions[$id]->getCorrectAnswer() == $answer;

        return $correctAnswer ? $questions[$id]->getPoints() : 0;
    }
}

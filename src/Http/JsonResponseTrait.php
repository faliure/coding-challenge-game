<?php

namespace Ucc\Http;

trait JsonResponseTrait
{
    public function jsonResponse($data, int $statusCode = 200): void
    {
        header('Session-Id: ' . session_id());
        header('Content-Type: application/json; charset=utf-8');

        $body = json_encode($data);

        if (json_last_error() !== JSON_ERROR_NONE) {
            http_response_code(500);

            echo 'Failed to parse json response';

            exit(1);
        }

        http_response_code($statusCode);

        echo $body;

        exit(0);
    }
}
